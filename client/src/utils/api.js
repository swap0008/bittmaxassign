import Axios from "axios";

const endpoint = 'http://localhost:4000';

function getBuying(token) {
    return Axios({
        method: 'get',
        url: `${endpoint}/buying`,
        headers: {
            'x-auth-token': token
        }
    });
}

function getSelling(token) {
    return Axios({
        method: 'get',
        url: `${endpoint}/selling`,
        headers: {
            'x-auth-token': token
        }
    });
}

export function getData(token) {
    return Axios.all([getBuying(token), getSelling(token)]);
}

export function getToken(username) {
    return Axios({
        method: 'post',
        url: `${endpoint}/user`,
        headers: {'Access-Control-Allow-Origin': '*'},
        data: { username }
    });
}