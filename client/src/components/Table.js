import React from 'react';

function Table(props) {
    const { mirror, data, dir } = props;
    let sum = 0;
    const maxValue = data.map(d => d.quantity).reduce((a ,b) => a + b, 0);

    return (
        <div className="table-container">
            <div className="table-title">
                <p>{mirror ? 'Selling' : 'Buying'} ZEC</p>
                <p>Total: {Number.parseFloat(maxValue).toFixed(6)} ZEC</p>
            </div>
            <table border="0" style={dir ? {direction: 'rtl'} : {}}>
                <tbody>
                <tr>
                    <th>Sum</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                {data.map(d => (
                    <tr key={d._id}>
                        <td>
                            <div className="graph" style={{
                                background: `
                                    linear-gradient(to ${dir ? 'right' : 'left'}, 
                                    ${mirror ? 'rgba(252, 110, 107, 0.14)' : 'rgba(164, 249, 200, 0.14)'} 
                                    ${((sum = sum + d.quantity)/maxValue)*100}%, #1c2730 0%`
                            }}>
                            </div>
                            {Number.parseFloat(sum).toFixed(6)}
                        </td>
                        <td>{Number.parseFloat(d.quantity).toFixed(6)}</td>
                        <td style={
                            mirror ? {color: '#fd6f6c'} : {color: '#a4f9c8'} 
                        }>{Number.parseFloat(d.price).toFixed(2)}</td>
                    </tr>
                ))}
                </tbody>
            </table>  
        </div>
    );
}

export default Table;