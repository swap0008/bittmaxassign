import React from 'react';
import loadBar from '../static/loading.gif';

export default function({ msg }) {
    return (
        <div className="loading-section">
            <h1 className="user-heading-main">{msg}</h1>
            <img src={loadBar} alt="loading" width="100px" height="100px" />
        </div>
    );
}