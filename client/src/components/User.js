import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import LoadingBar from './LoadingBar';

class User extends Component {
    state = {
        username: '',
        loading: false
    }

    _handleToken = () => {
        const { username } = this.state;

        if(!username) {
            return alert('Error: username required.');
        }

        this.setState({ loading: true });
        setTimeout(() => {
            this.props._generateToken(username);
            this.props.history.replace('/');
        }, 2000);
    }

    render() {
        const { loading, username } = this.state;
        
        if (loading) return <LoadingBar msg={'Generating token'}/>

        return (
            <div className="user-container">
                <h1 className="user-heading-main">Generate Token</h1>
                <input 
                    type="text" 
                    name="username"
                    value={username}
                    onChange={(e) => this.setState({ username: e.target.value })}
                    placeholder="username" 
                    autoComplete="off" /><br />
                <div className="generate-btn" onClick={this._handleToken}>
                    <input type="submit" value="Generate" />
                    <i className="material-icons set-position">settings</i>
                </div>
            </div>
        );
    }
}

export default withRouter(User);