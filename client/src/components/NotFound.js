import React from 'react';

export default function () {
    return (
        <h1 style={{
            fontFamily: `'Varela Round', sans-serif`,
            textAlign: 'center',
            marginTop: `30vh`,
            fontSize: '50px'
        }}>404: Page not found!</h1>
    );
}