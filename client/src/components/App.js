import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { getData, getToken } from '../utils/api';
import User from './User';
import Dashboard from './Dashboard';
import LoadingBar from './LoadingBar';
import NotFound from './NotFound';

class App extends Component {
    state = {
        data: '',
        token: '',
        loading: false
    }

    componentDidMount() {
        const token = localStorage.getItem('token');   

        if (token) {
            this._generateData(token);
        }
    }

    _generateToken = (username) => {
        getToken(username)
            .then(res => {
                const token = res.headers['x-auth-token'];
                localStorage.setItem('token', token);
                this._generateData(token);
            })
            .catch(err => alert(err));
    }

    _generateData = (token) => {
        this.setState({ loading: true });
        getData(token)
            .then(res => this.setState({
                data: { buying: res[0].data, selling: res[1].data },
                token,
                loading: false
            }))
            .catch(err => {
                alert('Invalid token.');
                localStorage.clear();
                this.setState({ loading: false });
            });
    }

    _removeToken = () => {
        localStorage.clear();
        this.setState({ token: '' });
    }

    render() {
        const { data, token, loading } = this.state;

        if (loading) return <LoadingBar msg={'Verifying token'}/>

        return (
            <Router>
                <Switch>
                    {token
                        ? <Route exact path="/" render={() => <Dashboard data={data} clearToken={this._removeToken}/>} />
                        : <Route exact path="/" render={() => <User _generateToken={this._generateToken} />}/>}
                        <Route path="*" component={NotFound}/>
                </Switch>
            </Router>
        );
    }
}

export default App;