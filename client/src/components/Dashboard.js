import React, { Component } from 'react';
import Table from './Table';
import swap from '../static/swap.png';

class Dashboard extends Component {
    state = {
        buying: [],
        selling: [],
        mirror: true
    }

    componentDidMount() {
        const { buying, selling } = this.props.data;

        this.setState({
            buying,
            selling
        });
    }

    _toggleData = () => {
        this.setState((state) => ({
            buying: state.selling,
            selling: state.buying,
            mirror: !state.mirror
        }));
    }

    render() {
        const { buying, selling, mirror } = this.state;

        return (
            <div className="container">
                <div className="swap-img" onClick={this._toggleData}>
                    <img src={swap} alt="swap" />
                </div>
                <Table data={buying} mirror={!mirror} dir={false} />
                <Table data={selling} mirror={mirror} dir={true} />
                <button onClick={() => this.props.clearToken()}>Clear token</button>
            </div>
        );
    }
}

export default Dashboard;