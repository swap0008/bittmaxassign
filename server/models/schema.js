const Joi = require('joi');
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    quantity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        require: true
    }
});

function dataValidation(data) {
    const schema = {
        quantity: Joi.number().required(),
        price: Joi.number().required()
    }

    return Joi.validate(data, schema);
}

module.exports = {
    dataValidation,
    schema
}