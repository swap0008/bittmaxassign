const { schema } = require('./schema');
const mongoose = require('mongoose');

const Selling = mongoose.model('Selling', schema);

module.exports = Selling;