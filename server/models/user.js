const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose= require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    isAdmin: Boolean
});

userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'));
    return token;
}

const User = mongoose.model('User', userSchema);

function validateUser(user) {
    const schema = {
        username: Joi.string().min(1).max(255).required()
    }

    return Joi.validate(user, schema);
}

module.exports = {
    User,
    validateUser
};