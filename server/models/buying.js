const { schema } = require('./schema');
const mongoose = require('mongoose');

const Buying = mongoose.model('Buying', schema);

module.exports = Buying;