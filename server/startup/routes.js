const express = require('express');
const cors = require('cors');
const error = require('../middleware/error');
const buying = require('../routes/buying');
const selling = require('../routes/selling');
const user = require('../routes/user');

module.exports = function(app) {
    app.use(cors({ exposedHeaders: 'x-auth-token' }));
    app.use(express.json());
    app.use('/buying', buying);
    app.use('/selling', selling);
    app.use('/user', user);
    app.use(error);
}