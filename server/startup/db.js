const mongoose = require('mongoose');
const config = require('config');

module.exports = function () {
    const uri = `mongodb+srv://swap0008:${config.get('dbPass')}@cluster0-xaaf4.mongodb.net/test?retryWrites=true&w=majority`;
    mongoose.connect(uri, { useNewUrlParser: true, useFindAndModify: false }) 
        .then(() => console.log('Connected to MongoDB...'))
        .catch(() => {
            console.error('Could not connect to MongoDB');
            process.exit(2);
        });
}