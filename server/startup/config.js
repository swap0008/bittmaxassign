const config = require('config');

module.exports = function () {
    if (!config.get('jwtPrivateKey')) {
        console.error('Fatal Error: jwtPrivateKey is not defined.');
        process.exit(1);
    }
    
    if(!config.get('dbPass')) {
        console.error('Fatal Error: MongoDB password is not set.');
        process.exit(1);
    }
}