require('express-async-errors');
const express = require('express');
const app = express();

require('./startup/config')();
require('./startup/routes')(app);
require('./startup/db')();

app.listen(4000, () => console.log(`Listening to port 4000...`));

