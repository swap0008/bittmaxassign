const jwt = require('jsonwebtoken');
const config = require('config');
const { User } = require('../models/user');

module.exports = async function (req, res, next) {
    const token = req.header('x-auth-token');

    if(!token) return res.status(401).send('Access denied. No token provided');

    try {
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));

        const { _id } = decoded;
        const user = await User.findById(_id);
        if (!user) return res.status(401).send('Unauthorized user. User not Registered.');

        req.user = user;
        next();
    }
    catch (ex) {
        res.status(400).send('Invalid token');
    }
}