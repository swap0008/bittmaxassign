const _ = require('lodash');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const { dataValidation } = require('../models/schema');
const Buying = require('../models/buying');
const express = require('express');
const router = express.Router();

router.get('/', auth, async (req, res) => {
    const buying = await Buying.find();
    res.send(buying);
});

router.post('/', [auth, admin], async (req, res) => {
    const { error } = dataValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let buying = new Buying(_.pick(req.body, ['quantity', 'price']));
    buying = await buying.save();
    res.send(buying);
});

module.exports = router;