const auth = require('../middleware/auth');
const { User, validateUser } = require('../models/user');
const express = require('express');
const router = express.Router();

router.get('/me', auth, (req, res) => {
    res.send(req.user);
});

router.post('/', async (req, res, next) => {
    const { error } = validateUser(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let { username } = req.body;
    username = username.toLowerCase();

    let user = await User.findOne({ username });
    if (!user) {
        user = new User({ username, isAdmin: false });
        user = await user.save();
    }

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(user);
});

module.exports = router;