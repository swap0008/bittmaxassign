const _ = require('lodash');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const { dataValidation } = require('../models/schema');
const Selling = require('../models/selling');
const express = require('express');
const router = express.Router();

router.get('/', auth, async (req, res) => {
    const selling = await Selling.find();
    res.send(selling);
});

router.post('/', [auth, admin], async (req, res) => {
    const { error } = dataValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let selling = new Selling(_.pick(req.body, ['quantity', 'price']));
    selling = await selling.save();
    res.send(selling);
});

module.exports = router;